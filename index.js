/* eslint-env node */
'use strict';
const fs = require('fs-extra');
const execSync = require('child_process').execSync;
const { exec } = require('child_process');
const xmldom = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

module.exports = {
  name: 'realifex-corber-utils',
  includedCommands: function() {
    return {
      helloCommand: {
        name: 'change-mobile-target',
        description: 'A test command that says hello',
        availableOptions: [
          { name: 'platform', type: String, default: 'ios', aliases: ['p'] },
          { name: 'application', type: String, aliases: ['a']}
        ],
        run: function(commandOptions, rawArgs) {
          if (!commandOptions.application) {
            console.error('\nPlease specify the application name:\n$ ember-change-mobile-target -p ios -a personal\n')
          } else {
            updateFiles(commandOptions);
          }
        }
      }
    }
  }
};

var updateFiles = function(commandOptions) {
  const config = require(process.cwd() + '/config/corber.js');

  const appConfig = config(commandOptions.application);
  if (!appConfig) {
    console.log('\nInvalid application name. Check config/corber.js.\n');
  } else {
    processIconsAndSplashes(appConfig, commandOptions.application, commandOptions.platform);
    updateCordovaPackageFile(appConfig, commandOptions.platform);
    updateCordovaConfigFile(appConfig);
    remakeMobileProject(appConfig, commandOptions.platform);
    if (commandOptions.platform === 'ios') {
      copyiOSSplashScreen(appConfig);
    }
  }
}

var copyiOSSplashScreen = function(config) {
  console.log(config)
  console.log('Copying CDVLaunchScreen.storyboard to the Xcode project folder...');
  fs.copySync(`${process.cwd()}/corber/build-assets/ios/CDVLaunchScreen.storyboard`, process.cwd() + `/corber/cordova/platforms/ios/${config.name}/CDVLaunchScreen.storyboard`, { overwrite: true });
  console.log('Copying splash screen images to the Xcode project folder...');
  fs.copySync(`${process.cwd()}/corber/build-assets/ios/${config.build_assets_folder}/xcassets/splash-logo.imageset`, process.cwd() + `/corber/cordova/platforms/ios/${config.name}/Images.xcassets/splash-logo.imageset`, { overwrite: true });
  fs.copySync(`${process.cwd()}/corber/build-assets/ios/${config.build_assets_folder}/xcassets/splash-footer.imageset`, process.cwd() + `/corber/cordova/platforms/ios/${config.name}/Images.xcassets/splash-footer.imageset`, { overwrite: true });
  console.log('Removing alpha channel from icons (requires ImageMagick)');
  execSync(`find corber/cordova/platforms/ios/${config.name}/Images.xcassets/AppIcon.appiconset/ -name "*.png" -exec convert "{}" -alpha off "{}" \\;`)
}

var processIconsAndSplashes = function(appConfig, application, platform) {
  fs.copySync(`${process.cwd()}/${appConfig.icon}`, process.cwd() + '/corber/icon.svg');
  console.log('\n'+ appConfig.icon + ' -> corber/icon.svg\nGenerating ' + application + ' icons for ' + platform + '...');
  execSync(`corber make-icons --platform ${platform}`)
  console.log('Icons generated successfully.');

  if (platform === 'android') {
    const splash = appConfig['splash_' + platform];
    fs.copySync(`${process.cwd()}/${splash}`, process.cwd() + '/corber/splash.svg')
    console.log(splash + ' -> corber/splash.svg\nGenerating ' + application + ' splash screens for ' + platform + '...');
    execSync(`corber make-splashes --platform ${platform}`);
    console.log('Splash screens generated successfully.');

    console.log('Setting all splashs to \'port-xhdpi\'');
    let cordovaConfig = fs.readFileSync(`${process.cwd()}/corber/cordova/config.xml`, 'utf-8');
    const doc = new xmldom().parseFromString(cordovaConfig, 'application/xml');
    const platforms = doc.getElementsByTagName('platform');
    let android = null;
    for (var p = 0; p < platforms.length; p ++) {
      if (platforms[p].getAttribute('name') === 'android') {
        android = platforms[p];
        break;
      }
    }
    if (android) {
      const splashs = android.getElementsByTagName('splash');
      if (splashs) {
        for (var s = 0; s < splashs.length; s ++) {
          splashs[s].setAttribute('src', 'res/screen/android/port-xhdpi.png');
        }
      }
    }
    const str = xmlserializer.serializeToString(doc.documentElement);
    fs.writeFileSync(`${process.cwd()}/corber/cordova/config.xml`, str);
    console.log('Done all splashs to \'port-xhdpi\'');
  }
}

var updateCordovaPackageFile = function(appConfig, platform) {
  console.log('Updating corber/cordova/package.json...');
  let cordovaPackage = JSON.parse(fs.readFileSync(`${process.cwd()}/corber/cordova/package.json`, 'utf-8'));
  cordovaPackage.name = appConfig.bundle_id;
  cordovaPackage.displayName = appConfig.name;
  cordovaPackage.cordova.plugins["cordova-plugin-app-name"].APP_NAME = appConfig.name
  fs.writeFileSync(`${process.cwd()}/corber/cordova/package.json`, JSON.stringify(cordovaPackage));
  console.log('Done updating corber/cordova/package.json.');

  console.log(`Updating corber/cordova/plugins/fetch.json...`);
  try {
    let pluginsPackage = JSON.parse(fs.readFileSync(`${process.cwd()}/corber/cordova/plugins/fetch.json`, 'utf-8'));
    let pluginsPackageAppName = pluginsPackage['cordova-plugin-app-name'];
    if (pluginsPackageAppName) {
      pluginsPackageAppName.APP_NAME = appConfig.name;
      if (pluginsPackageAppName.variables) {
        pluginsPackageAppName.variables.APP_NAME = appConfig.name;
      }
    }
    fs.writeFileSync(`${process.cwd()}/corber/cordova/plugins/fetch.json`, JSON.stringify(pluginsPackage));
  } catch (e) {}
  console.log(`Done corber/cordova/plugins/fetch.json...`);
}

var updateCordovaConfigFile = function(appConfig) {
  console.log('Updating corber/cordova/config.xml...');
  let cordovaConfig = fs.readFileSync(`${process.cwd()}/corber/cordova/config.xml`, 'utf-8');
  const doc = new xmldom().parseFromString(cordovaConfig, 'application/xml');
  const widget = doc.getElementsByTagName('widget');
  widget[0].setAttribute('id', appConfig.bundle_id);

  const androidServicesFile = doc.getElementById('android-services-file');
  androidServicesFile.setAttribute('src', appConfig.google_services_file);

  const name = doc.getElementsByTagName('name')
  name[0].textContent= appConfig.name;

  doc.getElementById('ios-display-name').textContent = appConfig.ios_name;
  // doc.getElementById('android-display-name').textContent = appConfig.name;
  // doc.getElementById('cordova-plugin-app-name').setAttribute('value', appConfig.name);

  const str = xmlserializer.serializeToString(doc.documentElement);
  fs.writeFileSync(`${process.cwd()}/corber/cordova/config.xml`, str)
  console.log('Done updating corber/cordova/config.xml.');
}

var remakeMobileProject = function(appConfig, platform) {
  console.log(`Removing old ${platform} project from corber/cordova/platforms/${platform}`);
  fs.removeSync(`${process.cwd()}/corber/cordova/platforms/${platform}`);
  fs.removeSync(`${process.cwd()}/corber/cordova/plugins/${platform}.json`);
  console.log(`Recreating ${platform} project in corber/cordova/platforms/${platform}\n--------------------`);
  const addPlatformResult = execSync(`corber platform add ${platform}`);
  console.log(addPlatformResult.toString('utf8') + '\n--------------------\n')
}